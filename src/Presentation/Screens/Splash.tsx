
import { Image, View, Text } from 'react-native';
import { Images, ScreenDimensions } from '../../shared/constants';
import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import HomeScreen from '../../navigation/main';

const SplashScreen = () => {
    const [isReady, setIsReady] = useState(false)

    useEffect(() => {
        setTimeout(() => {
            setIsReady(true);
        }, 2000);
    }, []);

    const SplashView = () => {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    right: 0,
                    left: 0,
                }}>
                <Image
                    source={Images.SPLASH}
                    style={{
                        flex: 1,
                        width: ScreenDimensions.WINDOW_WIDTH,
                        height: ScreenDimensions.WINDOW_HEIGHT,
                    }}
                    resizeMode="stretch"
                />
            </View>
        );
    };

    return (
        <NavigationContainer>
            <>
                {!isReady && <SplashView />}
                {isReady && <HomeScreen />}
                <View
                    style={{
                        height: ScreenDimensions.WINDOW_HEIGHT / 18,
                        width: ScreenDimensions.WINDOW_WIDTH,
                        position: 'absolute',
                        bottom: 0,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                </View>
            </>
        </NavigationContainer>
    );
};

export default SplashScreen